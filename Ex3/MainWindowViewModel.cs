﻿using Prism.Commands;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Ex3
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        public DelegateCommand WebCommand { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;
        private string size;    
        private string url;

        public string URL
        {
            get { return url; }
            set
            {
                url = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("URL"));
            }
        }
        public string Size
        {
            get { return size; }
            set
            {
                size = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("Size"));
            }
        }
        public MainWindowViewModel()
        {
            WebCommand = new DelegateCommand(GetData);
            Size = "";
            URL = "";
        }
        private void GetData()
        {
             Task.Run(() =>
            {
                SafeInvoke(() => Size = "Please Wait..");
                WebRequest webRequest = WebRequest.Create(URL) ;
                WebResponse response = webRequest.GetResponse();
                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    string text = reader.ReadToEnd();
                    SafeInvoke(() => { Size = text.Length.ToString(); });

                }
            });
        }
        private void SafeInvoke(Action uiWork)
        {
            if (App.Current.Dispatcher.CheckAccess())
            {
                uiWork.Invoke();
                return;
            }
            App.Current.Dispatcher.BeginInvoke(uiWork);
        }
    }
}
