﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Ex1
{
    public class ValueConverterForHeight : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((double)value <= 0.25 * 200 + 50)
                return "SMALL";
            if ((double)value <= 0.5 * 200 + 50)
                return "MEDIUM";
            if ((double)value <= 0.75 * 200 + 50)
                return "LARGE";
            return "EXTRA LARGE";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
