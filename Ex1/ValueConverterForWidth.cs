﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Ex1
{
    public class ValueConverterForWidth : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((double)value <= 0.25 * 190 + 110)
                return "SMALL";
            if ((double)value <= 0.5 * 190 + 110)
                return "MEDIUM";
            if ((double)value <= 0.75 * 190 + 110)
                return "LARGE";
            return "EXTRA LARGE";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
