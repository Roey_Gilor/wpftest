﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace Ex4
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void dialogBtn_Click(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            DialogResult result = dialog.ShowDialog();
            if (!string.IsNullOrEmpty(result.ToString()))
            {
                txtPath.Text = dialog.SelectedPath;
            }
        }

        private void searchBtn_Click(object sender, RoutedEventArgs e)
        {
            SafeInvoke(() =>
            {
                listResult.Items.Clear();
                progressSearch.Value = 0;
                progressSearch.Maximum = Directory.GetFiles(txtPath.Text, "*.*", SearchOption.AllDirectories).
                Where((x) => x.Contains(txtsearch.Text)).Count();
            });
            FindFiles(txtPath.Text);
        }
        private async void FindFiles(string path)
        {
            await Task.Run(() =>
            {
                string[] files = Directory.GetFiles(path);
                foreach (string file in files)
                {
                    SafeInvoke(() =>
                    {
                        if (file.Contains(txtsearch.Text))
                        {
                            listResult.Items.Add(file);
                            progressSearch.Value++;
                        }
                    });
                    Thread.Sleep(500);
                }
                string[] folders = Directory.GetDirectories(path);
                foreach (var item in folders)
                {
                    FindFiles(item);
                }
            });
        }
        private void SafeInvoke(Action uiWork)
        {
            if (Dispatcher.CheckAccess())
            {
                uiWork.Invoke();
                return;
            }
            Dispatcher.BeginInvoke(uiWork);
        }
    }
}
